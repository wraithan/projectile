+++
title = "Projects"
weight = 0
sort_by = "weight"
+++

I have a small pile of projects, and this site hopes to catalog the hardware ones and provide public docs of design and build processes.
+++
title = "RC Car"
weight = 2
description = "Upgrading a late 80s RC car"
+++

An RC car I got out of my parent's basement. I remember my dad racing it around on the street and beach growing up and I'd like to revive it. The bulk of the project to start with will be the conversion from FM to 2.4GHz. Later I hope to add smarts, especially for the disconnected case.

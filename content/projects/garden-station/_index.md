+++
title = "Garden Station"
weight = 1
description = "Backyard garden and weather monitoring"
+++

Backyard weather monitoring and garden monitoring station. This may get split into two separate projects, my parents would like the weather monitoring setup, but don't need the garden setup. I'd like both. So for now I'm going to design it as a single setup where the garden monitoring kit is optional.

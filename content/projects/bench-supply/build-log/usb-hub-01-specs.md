+++
title = "USB Hub Part 1 - Specs"
date = 2020-01-03
+++

Time to figure out the specs and pick a few parts for the USB hub. Choosing parts for a hardware project is similar to choosing libraries for a software project. Working through desired features and existing constraints can help streamline this process, narrowing down the features you have to look at. At the same time, the tail always wags the dog a bit, meaning the part (or library) choice influences the specs.

<!-- more -->

The center of this whole project is the ability to power a lot of devices, even when they draw significant current on the USB lines. This hub will power my projects as I work on them, meaning it'll need diagnostic and safety features not always or generally available on hubs. Since I'll be doing projects with microcontrollers, I'll want to have the data lines hooked up to my computer. This increases my desire for safety features, the last thing I want is 110VAC mains getting fed into my computer's USB port because I messed up with a relay or something else terrible.

If I had to rank those priorities, they'd look something like: safety, diagnostics, power, data. They're all crucial and have their minimums, but if I have to lower power from 1.5A to 1.2A for diagnostics I will. If sticking to USB 2.0@480mbps means sacrificing power diagnostics or lowering max power, I'll drop to USB 2.0@12mbps. So with all that in mind, lets set some aspirations!

From what I understand of the USB 2.0 spec, it can negotiate 1.5A of current so I'd like to provide at least that on each external port. I'd also like to be able to dynamically set the current limit on a port, so if I'm working on something I know shouldn't get more than 100mA of current I can set that. Being able to see the current draw (and voltage) of each external port would also be nice. External port is an important note, I plan on having a heavy microcontroller in the hub, probably a [Particle Argon](https://docs.particle.io/argon/) which will be connected via an internally routed port and be the brains of the operations. Once I expand this hub, there'll be multiple USB hub ICs chained together, needing far less current and safety than the external ports.

Overcurrent, short, and reverse polarity protection are things I'll want on each port. I'm still researching each of these topics, overcurrent I can use a shunt to measure and as part of a feedback loop, and I'll want a fuse to protect if the current is too high for the feedback loop. I'm hoping to use a fuse per "bank" which would be all the external ports for a given USB hub IC, but the first design is a single hub IC to learn about making hubs so it'll be a single fuse for the whole hub. Diodes are the natural way to deal with reverse polarity, I'll need to dig up the video I saw where they used diodes to allow the reverse polarity to power a transistor to shut down the power circuit. I could do similar but include a diagnostic signal so I get feedback that I messed up.

Ok, so that's a bunch of concerns, lets make a list of exact specs and see how they survive the part choices and evolve as I learn how easy or hard various features are to achieve.

### Vision of the Specs:

* External USB Ports x 3
    * USB 2.0@480mbps
    * 0mA-2A selectable current (non-linear resolution is alright)
    * Diagnostic outputs
        * Current draw
        * Voltage draw
        * Errors (Overcurrent, short, reversed, etc.)
* Internal USB Port x 1
    * [Feather](https://learn.adafruit.com/adafruit-feather/feather-specification) port (I guess this maybe considered a USB Hub Featherwing? lol)
* Power supply
    * Home - 5V 20A lines from a 480W ATX Power Supply
    * Portable (one of the following)
        * 5V 5A wall adapter (lower limits, relatively cheap to get)
        * 16-20V 3-4A Laptop charger + buck converter (I have a few chargers laying around but would need the buck converter)
    * Fuse but not sure exactly where
        * Maybe on input to protect everything in case of a fault outside too.
        * Maybe just across the external port power lines
        * Maybe both?
* Outputs
    * Display
        * Graphic, so I can show graphs
        * Color because I'm fancy
        * SPI or I2C to make it easier to change out
        * I'm undecided on if I want one big display or a couple smaller displays
    * Status LED(s) for each port and the hub itself
        * RGB + patterns goes a long way
        * Better ambient notification than the display, I'll notice color/intensity change in my periphery better than an indicator on a display.
* Inputs
    * At least 1 rotary encoder
    * Toggles for each port
    * Touchscreen is possible, depends on display
    * Push buttons
    * USB Serial
        * Then I can use tools running on the connected computer to control it.
    * WiFi
        * Since I'm going to use a feather footprint and most of them have WiFi
        * I'll probably use [Particle Cloud](https://docs.particle.io/reference/device-os/firmware/argon/#cloud-functions) since that'll be easy with the Argon, but maybe I'll explore other options as well.
* Firmware
    * Rust!
        * So, I don't currently have a working build process for using rust with Particle hardware as a user firmware.
        * This is mostly for my own amusement and preference
        * Realistically this will be C++ at first
    * Control current for each port
    * Drives data to the displays
    * Can read in the per port diagnostic data.
    * Data logging to an SD card
        * Conveniently a lot of displays have micro SD card slots to hold images, often accessible over then same SPI or I2C bus.
        * Can always fall back to using an [OpenLog](https://www.sparkfun.com/products/13712) that I have a few of from old projects a couple years ago.

I was hoping to pick parts in this post but this post has already carried on quite a bit. My next post I'll go into part selection and specifying anything I forgot.
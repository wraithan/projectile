+++
title = "Getting Started!"
date = 2019-12-31
+++
So, I've picked up two 430W ATX power supplies from [Free Geek](https://www.freegeek.org/) for $15 each. These will form the backbone of this whole project, each have 3.3v, 5v, and 12v rails of various types and current availability. This matches what I most commonly need even outside of a computer case. Most development boards are geared for USB which is 5v, those same development boards mostly step that 5v down to 3.3v, bigger fans, lighting, etc. all can run on 12v.

<!-- more -->

A main driver of this project is USB power, more devices with less wires plugged into the wall to power them. To that end I'm going to make my own USB hub including data. I've settled on the [TUSB4041I](http://www.ti.com/lit/ds/symlink/tusb4041i.pdf) as my main USB IC. The first hub will be a 3 port 1A per port hub with some extra features. I hope to scale this up to using 5 USB hub chips giving 15 external ports, with a subset having a higher amount of current available.

I found this [video series](https://www.youtube.com/watch?v=rdkODwZjlqI&list=PLPGhq7KGBLN0sR7ghe55hiBDOaAkCSf_G) that has someone who has designed a hub before going through their process. I've only watched the first hour or two, but I've already learned a few things about USB hub design, like power should be toggled on the power line, not the ground line.

Next post I'll go into the USB hub in detail, also I want to keep track of some sort of build capabilities so far and designed capabilities. Both will grow and change with time, but hopefully it'll provide some interesting insight.

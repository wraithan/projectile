+++
title = "USB Hub Part 2 - Parts"
date = 2020-02-02
+++

I've started designing the hub in [KiCad](https://kicad-pcb.org/) which has lead to me picking various parts. It has also made me realize I missed a few things the first time through the datasheet for the [TUSB404I](http://www.ti.com/lit/ds/symlink/tusb4041i.pdf) which is the IC I'm using for USB support. Things like the timing of the various power supply lines becoming stable is important. Also the I2C interface on the hub is for providing an EEPROM to the hub for it's config.

<!-- more -->

So lets set the context by providing a list of parts that I've chosen so far.

* [TUSB404I (USB Hub IC)](http://www.ti.com/lit/ds/symlink/tusb4041i.pdf)
* [MIC2039 (Power Switch)](http://ww1.microchip.com/downloads/en/DeviceDoc/MIC2039-High-Accuracy-High-Side-Adjustable-Current-Limit-Power-Switch-20005540A.pdf)
* [MCP4452 (Digital Rheostat)](http://ww1.microchip.com/downloads/en/DeviceDoc/MCP4451.pdf)
* [TLV1117LV (1.1V Voltage Regulator)](http://www.ti.com/lit/ds/symlink/tlv1117lv.pdf)
* [Particle Argon (Microcontroller and Connectivity)](https://docs.particle.io/datasheets/wi-fi/argon-datasheet/)

Everything else I've chosen are passives and connectors to support these parts. I still need to figure out output such as the screen and LED indicators. As well as diagnostic stuff such as shunts for measuring actual current draw on each port.

The TUSB404I was chosen for a few reasons. First was that Texas Instruments tends to have detailed datasheets, which as the central IC of the build and wanting a good typical application circuit was important. If you've never designed a board before (like myself) then perhaps you also didn't know typical application circuits were provided in a lot of datasheets.

The best typical applications include a list of performance characteristics, supplemental ICs, and values for the passive components like resistors and capacitors. The one with TUSB404I includes external power like I desire, but the current limit on it is a fixed 500mA, while I want an adjustable current limit. That lead me to pick MIC2039 which is power switch with adjustable current limit, and more or less drops right into the typical application for the TUSB404I.

Another reason why I picked the TUSB404I was the I2C on it. Though, as I mentioned at the start I misunderstood what this was capable of. I had assumed there would be some sort of command interface over I2C, but it turns out it uses I2C to give access to an external EEPROM. I still haven't fully understood how much dynamic control I'll have via the EEPROM, since I'll be programming it with the Argon microcontroller. This is something I'll have to experiment with.

Finally the TUSB404I is a USB 2.0 480mbps capable IC, which means faster USB stuff like webcams and USB hard drives will work. A lot of the examples out there of folks making their own hub stick to 12mbps, either because the IC already has a crystal/oscillator built in so it saves on other components. Or because the lower speed is significantly easier to achieve since noise affects it differently. Since this is a personal project and I can fall back to 12mbps if the external crystal doesn't work out, I figure I might as well try.

Moving onto the MIC2039 power switch, one of the hard thing in finding the right switch was finding one that came in package I liked with the the 2A current limit I wanted. Once I narrowed down to that, I had a look into how to actually control the current limit. This is when I started diving into the world of digital potentiometers. I found out that getting decent accuracy isn't cheap, but possible. Also I learned if you're just connecting two pins, the wiper (the output of the resistors) and the input, then it is called a rheostat and those come in slightly smaller packages which is nice.

I decided on using the MCP4452 digital rheostat, which then provided feedback into the selection of the power switch. Finding one that wants a resistance range the rheostat I selected can provide. I'm still not fully settled on this rheostat because the accuracy seems a bit low (±20%) but I want to see how well that works with the switch before changing to a higher cost one that I'd need to set fixed points in but give me ±1% accuracy. The MIC2039 has a resistance range up to 2,000 ohm which means I can use the lowest resistance version of the rheostat, which goes to 5,000 ohm.

Also in the power realm is the 1.1V regulator. The TUSB404I takes 2 levels of power, 1.1V and 3.3V, and when booting up requires either that those lines have the 1.1V become available before the 3.3V and using a capacitor to control how long the reset line takes to pull up. For this I mostly just needed a package I could solder reasonably, I ended up with a Texas Instruments one mostly because I'm familiar with their datasheets. The timing thing has been what held me up on the circuit design though.

I found I don't have a good grasp of RC circuits or Resistor Capacitor circuits, which can be used to generate a delay. But that's content for another build log. If I spend more time on this project later today I'll try to include a few images of the schematic so far.
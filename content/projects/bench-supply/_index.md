+++
title = "Bench Supply"
weight = 0
description = "Building a custom power setup for my workbench"
+++

I need a fair amount of power on my bench:

* My collection of 10+ work devices, powered off USB
* Working on devices that range from 3.3v to 12v DC
* Charging various batteries (NiCd, LiPo, etc.)
* Fans for clearing smoke/fumes while soldering, etc.

And I have two 480 watt ATX power supplies that I intend to use to power all of that. Preferably just off one of them, since it should be more than enough.

The power output will be banked with fuses/switches per bank. This may spawn a subproject of making a proper USB hub with USB 2.1 ICs.

+++
title = "Wraithan's Project Shed"
+++

# Welcome to my Project Shed

This will be where I document my (hardware) projects, builds, and tools. Each project should come with a build log, theory of operation, resources, bill of materials and various other information about it. I hope to achieve the following:

* Keep my projects a bit more organized over time, especially as I take breaks and switch tasks often.
* Share what I've learned in a way that helps others.
* Show growth over time, publishing failures is as important, if not more, than successes when documenting the history of a project.
* Have fun showing off the neat things I work on.

Most things are purely scaffolded for now, check back and hopefully more of this will be filled out.
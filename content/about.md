+++
title = "About"
+++
I'm Wraithan, my occupation is software developer, I play and make games as a hobby, and have had an on and off relationship with hardware hacking over the years. Recently I was employed by [Particle](https://particle.io) to work on their developer tools. This has been the primary driver for me to get back into making electronics for fun. On that note, all opinions expressed on this site are my own and not my employer's. There may be projects that use my employer's hardware or I build to support my home working environment, you should not take this as advise for working with my employer's (and anyone else's) products, it is purely documentation of what I've done.

As for the "shed" it is purely metaphorical at this point, the majority of my hardware work is done at home in my office that is a shared space for PC gaming and my partner's desk. Maybe with time I'll move somewhere I can build out a separate lab area, and if it has a separate entrance from my house, you better believe I'll call it my "Project Shed"

I'm an absolute amateur when it comes to building stuff with hardware, but I've taken things to bits since I was little and have always enjoyed the many things that go into hardware development. I love trying to figure out how something was made, or why they did something weird or different. I'm hoping that learning publicly enables others to not have to make the same mistakes and instead make their own new and exciting ones.

function initToggleMenu() {
  var $menu = document.querySelector("nav");
  var $menuIcon = document.querySelector(".menu-icon");
  var $page = document.querySelector("article");
  $menuIcon.addEventListener("click", function() {
    $menu.classList.toggle("menu-hidden");
    $page.classList.toggle("page-without-menu");
  });
}

function debounce(func, wait) {
  var timeout;

  return function () {
    var context = this;
    var args = arguments;
    clearTimeout(timeout);

    timeout = setTimeout(function () {
      timeout = null;
      func.apply(context, args);
    }, wait);
  };
}

if (document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
  initToggleMenu();
} else {
  document.addEventListener("DOMContentLoaded", function () {
    initToggleMenu();
  });
}
